import {Song} from './song';
export class Artist {
  public id;
  public name;
  public lastname;
  public instagram;
  public profile;
  public numberOfSongs;
  public profileLocal;
  songs:Song[];
}
