import {Injectable} from '@angular/core';
@Injectable()
export class ConfigService {
  public API_URL = 'http://api-muzyka.aio.kg/api';
  public APIS_URL = 'http://api-muzyka.aio.kg/apis';
  public URL = 'http://api-muzyka.aio.kg';

  // public API_URL = 'http://muzyka.api/api';
  // public APIS_URL = 'http://muzyka.api/apis';
  // public URL = 'http://muzyka.api';
}
